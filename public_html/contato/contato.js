
$(function() {

 $("input,textarea").jqBootstrapValidation(
    {
     preventSubmit: true,
     submitError: function($form, event, errors) {
   
     },
     submitsucesso: function($form, event) {
      event.preventDefault();
       var name = $("input#nome").val();  
       var email = $("input#email").val(); 
       var message = $("textarea#mensagem").val();
        var firstName = name; 
        if (firstName.indexOf(' ') >= 0) {
	   firstName = name.split(' ').slice(0, -1).join(' ');
         }        
	 $.ajax({
              url: "contato.php",
            	type: "POST",
            	data: {name: name, email: email, message: message},
            	cache: false,
            	sucesso: function() {  
       
            	   $('#sucesso').html("<div class='alert alert-sucesso'>");
            	   $('#sucesso > .alert-sucesso').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            		.append( "</button>");
            	  $('#sucesso > .alert-sucesso')
            		.append("<strong>Sua mensagem foi enviada. </strong>");
 		  $('#sucesso > .alert-sucesso')
 			.append('</div>');
 		
 		  $('#contactForm').trigger("reset");
 	      },
 	   error: function() {		

 		 $('#sucesso').html("<div class='alert alert-danger'>");
            	$('#sucesso > .alert-danger').html("<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;")
            	 .append( "</button>");
            	$('#sucesso > .alert-danger').append("<strong>Desculpa "+firstName+" parece que meu servidor de e-mail não está respondendo...</strong> Por favor enviar diretamente para <a href='mailto:exemplo@exemplo.com?Subject='>exemplo@exemplo.com</a>. Desculpe pela inconveniência!");
 	        $('#sucesso > .alert-danger').append('</div>');

 		$('#contactForm').trigger("reset");
 	    },
           })
         },
         filter: function() {
                   return $(this).is(":visible");
         },
       });

      $("a[data-toggle=\"tab\"]").click(function(e) {
                    e.preventDefault();
                    $(this).tab("show");
        });
  });
 

$('#name').focus(function() {
     $('#sucesso').html('');
  });
